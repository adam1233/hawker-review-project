//
//  RestaurantDataPersistanceTest.swift
//  hawker review project
//
//  Created by Kerk Wee Sin on 05/12/2015.
//  Copyright © 2015 Kai siang Tui. All rights reserved.
//

import XCTest
import CoreLocation

@testable import hawker_review_project
class RestaurantDataPersistanceTest: XCTestCase {
    
    var restaurant: Restaurants!
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
}
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    func testRestaurantData() {
    
        let defaults: NSUserDefaults = NSUserDefaults.standardUserDefaults()
        let restaurant: Restaurants = Restaurants.getRandomDummyRestaurants()
        let restaurantData: NSData = NSKeyedArchiver.archivedDataWithRootObject(restaurant)
        defaults.setObject(restaurantData, forKey: "testRestaurantData")
        defaults.synchronize()
        
        let readRestaurantData: NSData = defaults.objectForKey("testRestaurantData") as! NSData
        let readRestaurant: Restaurants? = NSKeyedUnarchiver.unarchiveObjectWithData(readRestaurantData) as? Restaurants
        
        XCTAssertNotNil(readRestaurant, "Should be able to read restaurant data from defaults and return an object")
        XCTAssertEqual(restaurant.picture, readRestaurant?.picture, "Names from restaurant and recreation park should be match")
    }
    
    func testWritingToFile() {
    
        let fileManager: NSFileManager = NSFileManager.defaultManager()
        let documentsDirectory: NSURL = fileManager.URLsForDirectory(NSSearchPathDirectory.DocumentDirectory, inDomains: NSSearchPathDomainMask.UserDomainMask).first!
        
        let filePath: NSURL = documentsDirectory.URLByAppendingPathComponent("data.plist")
        
        var restaurants: [Restaurants] = []
        restaurants.append(Restaurants(name: "Good", location: CLLocationCoordinate2DMake(2.0, 2.0), picture: UIImage(), rating: 4.5))
        
        var restaurantData: [NSData] = []
        for restaurant in restaurants {
        
            let data: NSData = NSKeyedArchiver.archivedDataWithRootObject(restaurant)
            restaurantData.append(data)
        }
        
        (restaurantData as NSArray).writeToURL(filePath, atomically: true)
        
        let fileExists: Bool = fileManager.fileExistsAtPath(filePath.path!)
        XCTAssert(fileExists, "File did Exists")
        
        NSLog("File Path: \(filePath.path)")
    }


    func testReadFromFile() {
        
        
        let fileManager: NSFileManager = NSFileManager.defaultManager()
        let documentsDirectory: NSURL = fileManager.URLsForDirectory(NSSearchPathDirectory.DocumentationDirectory, inDomains: NSSearchPathDomainMask.UserDomainMask).first!
        
        let filePath: NSURL = documentsDirectory.URLByAppendingPathComponent("data.plist")
        
        let restaurantData: [NSData] = NSArray(contentsOfURL: filePath) as! [NSData]
        var restaurants: [Restaurants] = []
        
        for data in restaurantData {
        
            let restaurant: Restaurants = NSKeyedUnarchiver.unarchiveObjectWithData(data) as! Restaurants
            restaurants.append(restaurant)
        }
        XCTAssert(restaurants.count == 1, "should be equal")
    
    }
    
      func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measureBlock {
            // Put the code you want to measure the time of here.
        }
    }
    
}
