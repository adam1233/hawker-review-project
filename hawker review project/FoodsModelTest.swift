//
//  FoodsModelTest.swift
//  hawker review project
//
//  Created by Kai siang Tui on 11/21/15.
//  Copyright © 2015 Kai siang Tui. All rights reserved.
//

import XCTest
@testable import hawker_review_project

class FoodsModelTest: XCTestCase {
    
    func testFoodRating(){
    
        let food1: Food = Food(name: "Nasi Lemak", cuisine: "Malay Food")
        food1.arrayOfRatings.append(3)
        food1.arrayOfRatings.append(4)
        food1.arrayOfRatings.append(5)
        food1.averageRating()
        
        
        let food2: Food = Food(name: "Laksa", cuisine: "Chinese Food")
        food2.arrayOfRatings.append(4)
        food2.arrayOfRatings.append(4)
        food2.arrayOfRatings.append(4)
        food2.arrayOfRatings.append(4)
        food2.arrayOfRatings.append(4)
        
        //var foods: [Food] = [food1, food2]
       
       XCTAssert(food1.averageRating() == 4)
        //XCTAssert(food1.rating > -1 && food1.rating <= 5)
        
        XCTAssert(food2.averageRating() == 4)
        
        
        
        
    }
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measureBlock {
            // Put the code you want to measure the time of here.
        }
    }

}
