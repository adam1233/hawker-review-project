//
//  Food.swift
//  hawker review project
//
//  Created by Kerk Wee Sin on 21/11/2015.
//  Copyright © 2015 Kai siang Tui. All rights reserved.
//

import Foundation
import UIKit
import MapKit

class Food {
    var name: String!
    var cuisine: String!
    var arrayOfRatings :[Int] = []
 
    
    init (name: String, cuisine: String){
    self.name = name
    self.cuisine = cuisine
 
    }
    
    func averageRating() ->Int{
        
        var sum:Int = 0
        let numberOfRatings: Int = arrayOfRatings.count
        
        for rating in arrayOfRatings {
            sum += rating
        }
        let averageRating = sum / numberOfRatings
        return averageRating
    }
}