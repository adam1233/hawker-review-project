//
//  Restaurents.swift
//  hawker review project
//
//  Created by Kerk Wee Sin on 21/11/2015.
//  Copyright © 2015 Kai siang Tui. All rights reserved.
//

import Foundation
import MapKit
import CoreLocation

class Restaurants: NSObject, NSCoding{
    var name: String!
    var food: [Food] = []
    var location: CLLocationCoordinate2D
    var foodListing: [Food] = []
    var restaurantRatings: [Int] = []
    var picture: UIImage!
    var arrayOfRatings:[Double] = []
    var rating: Double!
    
    func encodeWithCoder(aCoder: NSCoder) {
        let keyArchiever: NSKeyedArchiver = aCoder as! NSKeyedArchiver
        
        keyArchiever.encodeObject(self.name, forKey: "RESRestaurantName")
        keyArchiever.encodeObject(self.restaurantRatings, forKey: "RESRestaurantRatings")
        
        keyArchiever.encodeObject(self.location.latitude, forKey: "RESRestaurantLocationLatitude")
        keyArchiever.encodeObject(self.location.longitude, forKey: "RESRestaurantLocationLongitude")
        keyArchiever.encodeObject(self.rating, forKey: "RESRestaurantRating")
        
        if self.picture != nil {
            let imageData = UIImagePNGRepresentation(self.picture)
            keyArchiever.encodeObject(imageData, forKey: "RESRestaurantPicture")
        }
        
    }
    
    required convenience init?(coder aDecoder: NSCoder) {
        let keyedUnarchiver: NSKeyedUnarchiver = aDecoder as! NSKeyedUnarchiver
        let name = keyedUnarchiver.decodeObjectForKey("RESRestaurantName") as! String
        let latitude: Double = keyedUnarchiver.decodeObjectForKey("RESRestaurantLocationLatitude") as! Double
        let longitude: Double = keyedUnarchiver.decodeObjectForKey("RESRestaurantLocationLongitude") as! Double
        
        var loadedPicture: UIImage?
        if let imageData = keyedUnarchiver.decodeObjectForKey("RESRestaurantPicture") as? NSData {
            
            loadedPicture = UIImage(data: imageData)
        }
        
        let loadedRating: Double = keyedUnarchiver.decodeObjectForKey("RESRestaurantRating") as! Double
        
         self.init(name: name, location: CLLocationCoordinate2DMake(latitude, longitude), picture: loadedPicture, rating: loadedRating)
        
    }
    
    init(name: String, location: CLLocationCoordinate2D, picture:UIImage?, rating: Double) {
        self.location = location
        self.name = name
        self.picture = picture
        self.rating = rating
        
        super.init()
        
    }
    
    class func getRandomDummyRestaurants() -> Restaurants {
        var restaurantLists: [Restaurants] = []
        
        let restaurant1 = Restaurants(name:"Graceful Restaurant", location: CLLocationCoordinate2DMake(1.0, 2.0), picture: UIImage(named: "simplelife"), rating: 5.0)
        restaurantLists.append(restaurant1)
        let restaurant2 = Restaurants(name:"Jie Yuan Restaurant", location: CLLocationCoordinate2DMake(1.0, 2.0), picture: UIImage(named: "simplelife"), rating: 4.5)
        let restaurant3 = Restaurants(name:"Ling Zi Restaurant", location: CLLocationCoordinate2DMake(1.0, 2.0), picture: UIImage(named: "lingzi_restoran"), rating: 3.0)
        let restaurant4 = Restaurants(name:"Organic Vegetarian Restaurant", location: CLLocationCoordinate2DMake(1.0, 2.0), picture: UIImage(named: "simplelife"), rating: 4.0)
        let restaurant5 = Restaurants(name:"Secret Recipe Beyond Vege Restaurant", location: CLLocationCoordinate2DMake(1.0, 2.0), picture: UIImage(named: "simplelife"), rating: 3.5)
        let restaurant6 = Restaurants(name:"Simple Life Restaurant", location: CLLocationCoordinate2DMake(1.0, 2.0), picture: UIImage(named: "simplelife"), rating: 3.7)
        let restaurant7 = Restaurants(name:"Teik EE Vegetarian Restaurant", location: CLLocationCoordinate2DMake(1.0, 2.0), picture: UIImage(named: "simplelife"), rating: 3.3)
        let restaurant8 = Restaurants(name:"The Origin Vegetarian Restaurant", location: CLLocationCoordinate2DMake(1.0, 2.0), picture: UIImage(named: "simplelife"), rating: 4.5)
        let restaurant9 = Restaurants(name:"Together Vegetarian Restaurant", location: CLLocationCoordinate2DMake(1.0, 2.0), picture: UIImage(named: "simplelife"), rating: 4.3)
        let restaurant10 = Restaurants(name:"Veggie Grill Vegetarian Restaurant", location: CLLocationCoordinate2DMake(1.0, 2.0), picture: UIImage(named: "simplelife"), rating: 3.8)
        let restaurant11 = Restaurants(name:"Zhun San Yen Vegetarian Restaurant", location: CLLocationCoordinate2DMake(1.0, 2.0), picture: UIImage(named: "simplelife"), rating: 4)
        
        restaurantLists = [restaurant1, restaurant2, restaurant3, restaurant4, restaurant5, restaurant6, restaurant7, restaurant8, restaurant9, restaurant10, restaurant11]
        let index: Int = Int(arc4random_uniform(UInt32(restaurantLists.count)))
        let randomRestaurantList = restaurantLists[index]
        
        return randomRestaurantList
        
        
    }
    
    
    func averageRestaurantRating() ->Double{
        var sum: Double = 0
        let numberOfRatings = arrayOfRatings.count
        
        for rating in arrayOfRatings {
            
            sum += rating
            
        }
        let averageRestaurantRating: Double = Double(sum) / Double(numberOfRatings)
        
        return averageRestaurantRating
    }
}