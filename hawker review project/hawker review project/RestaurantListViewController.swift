//
//  RestaurantListViewController.swift
//  hawker review project
//
//  Created by Kai siang Tui on 11/20/15.
//  Copyright © 2015 Kai siang Tui. All rights reserved.
//

import UIKit

class RestaurantListViewController: UIViewController{
    
    
    @IBAction func editRestaurantButton(sender: UIBarButtonItem) {
        
        
        
    }
    var restaurants: [Restaurants] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        //preloadData()
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.navigationItem.leftBarButtonItem = self.editButtonItem()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        if let selectedIndexPath = self.tableView.indexPathForSelectedRow {
            self.tableView.deselectRowAtIndexPath(selectedIndexPath, animated: animated)
        
        }
    }
     
    override func setEditing(editing: Bool, animated: Bool) { 
        super.setEditing(editing, animated: animated)
        self.tableView.setEditing(editing, animated: animated)
        
    }
    func preloadData() {
    
        for (var i = 0; i <= 10; i++) {
            
            self.restaurants.append(Restaurants.getRandomDummyRestaurants())
            
        }
        self.tableView.reloadData()
    }
    
    @IBOutlet var restaurantRatingLabel: UILabel!
    
      @IBAction func addRestaurantButton(sender: UIBarButtonItem) {
        self.restaurants.append(Restaurants.getRandomDummyRestaurants())
        self.tableView.reloadData()

    }
    
    @IBOutlet weak var imageView: UIImageView!
    

    @IBOutlet weak var tableView: UITableView!

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "showAddRestaurantVC" {
        
            let navigatinVC: UINavigationController = segue.destinationViewController as! UINavigationController
            let targetAddRestaurantTopVC: AddRestaurantViewController = navigatinVC.topViewController as! AddRestaurantViewController
//            addRestaurantVC.delegate = self
            targetAddRestaurantTopVC.addRestaurantCompletionBlock = {(restaurant: Restaurants!) -> Void in
                
                self.restaurants.append(restaurant)
                self.tableView.reloadData()
                self.dismissViewControllerAnimated(true, completion: nil)
            }
        }
    }
}


extension RestaurantListViewController: UITableViewDataSource {

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.restaurants.count
    }

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell: RestaurantsCell = self.tableView.dequeueReusableCellWithIdentifier("RestaurantsCell") as! RestaurantsCell
       let restaurant = self.restaurants[indexPath.row]
        cell.restaurant = restaurant
        
        return cell
   }
    
}



extension RestaurantListViewController: UITableViewDelegate {
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let restaurantDetailsVC: RestaurantDetailsViewController = self.storyboard?.instantiateViewControllerWithIdentifier("RestaurantDetailsViewController") as! RestaurantDetailsViewController
        let restaurant: Restaurants = self.restaurants[indexPath.row]
        restaurantDetailsVC.restaurant = restaurant
        self.navigationController?.pushViewController(restaurantDetailsVC, animated: true)
    }
    
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == UITableViewCellEditingStyle.Delete {
        
            self.restaurants.removeAtIndex(indexPath.row)
            self.tableView.reloadData()
            
            
        }
    }
}

//extension RestaurantListViewController: AddRestaurantDelegate{
//
//    func viewController(vc: AddRestaurantViewController, didAddRestaurant restaurant: Restaurants!) {
//        self.restaurants.append(restaurant)
//        self.tableView.reloadData()
//    }
//
//}

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
//    }
//
//    override func didReceiveMemoryWarning() {
//        super.didReceiveMemoryWarning()
//        // Dispose of any resources that can be recreated.
//    }
//
//    // MARK: - Table view data source
//    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
//        // #warning Incomplete implementation, return the number of sections
//        return 0
//    }
//
//    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        // #warning Incomplete implementation, return the number of rows
//        return 0
//    }

    /*
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("reuseIdentifier", forIndexPath: indexPath)

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */


