//
//  AddRestaurantViewController.swift
//  hawker review project
//
//  Created by Kerk Wee Sin on 28/11/2015.
//  Copyright © 2015 Kai siang Tui. All rights reserved.
//

import UIKit
import MapKit

//protocol AddRestaurantDelegate {
//    func viewController (vc:AddRestaurantViewController, didAddRestaurant restaurant: Restaurants!)
//}


class AddRestaurantViewController: UIViewController, UINavigationControllerDelegate {
    
    @IBOutlet var addRestaurantImageButtonOutlet: UIButton!
    @IBOutlet var sliderValueLabel: UILabel!
    var location: CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: 1.0, longitude: 2.0)
    @IBOutlet var restaurantRatingSlider: UISlider!
    var restaurant: Restaurants?
    var addRestaurantCompletionBlock : ((restaurant: Restaurants!) -> Void)?
    var addRestaurantVC = RestaurantDetailsViewController()
    
    //var delegate:AddRestaurantDelegate?
    @IBOutlet var restaurantNameTextField: UITextField!
    
    @IBOutlet var restaurantImageView: UIImageView!
    @IBOutlet var restaurantRatingLabel: UILabel!
    @IBOutlet var restaurantNameLabel: UILabel!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.sliderValueLabel.text = "Rating: \(self.restaurantRatingSlider.value)"
    }
    
    
    
    @IBAction func addRestaurantImageButton(sender: UIButton) {
        showAddImageByCameraOrImageActionSheet()
    }
    
    
    func showAddImageByCameraOrImageActionSheet() {
        // create button localize string
        let cameraButtonTitle = NSLocalizedString("Camera", comment: "Camera")
        let photoLibraryButtonTitle = NSLocalizedString("Photo Library", comment: "Photo Library")
        let cancelButtonTitle = NSLocalizedString("Cancel", comment: "Cancel")
        
        let alertVC = UIAlertController(title: "Add Your Image Via", message: nil, preferredStyle: .ActionSheet)
        
        let cancelAction = UIAlertAction(title: cancelButtonTitle, style: .Cancel) { _ in
            
            
            print("cancel")
        }
        
        let cameraAction = UIAlertAction(title: cameraButtonTitle, style: .Default) { _ in
            
            let picker: UIImagePickerController = UIImagePickerController()
            if UIImagePickerController.isSourceTypeAvailable(.Camera) {
                picker.sourceType = .Camera
                picker.delegate = self
                picker.allowsEditing = true
                self.presentViewController(picker, animated: true, completion: nil)
                print("Using Camera")
            }
        }
        
        let photoLibraryAction = UIAlertAction(title: photoLibraryButtonTitle, style: .Default) { _ in
            let picker: UIImagePickerController = UIImagePickerController()
            picker.delegate = self
            picker.allowsEditing = true
            picker.sourceType = .PhotoLibrary
            self.presentViewController(picker, animated: true, completion: nil)
            print("Using PhotoLibrary")
        }
        
        alertVC.addAction(cameraAction)
        alertVC.addAction(photoLibraryAction)
        alertVC.addAction(cancelAction)
        
        if let popoverPresentationController = alertVC.popoverPresentationController {
            popoverPresentationController.sourceRect = self.addRestaurantImageButtonOutlet.frame
            popoverPresentationController.sourceView = view
            popoverPresentationController.permittedArrowDirections = .Down
        }
        presentViewController(alertVC, animated: true, completion: nil)
    }
    
    @IBAction func ratingChanged(sender: AnyObject) {
        self.restaurantRatingSlider.thumbTintColor = addRestaurantVC.variationColorOfText (Double(self.restaurantRatingSlider.value))
        self.sliderValueLabel.text = "Rating: \(round(self.restaurantRatingSlider.value))"
        
    }
    
    @IBAction func saveButton(sender: UIBarButtonItem) {
        if let name = self.restaurantNameTextField.text {
            let restaurant: Restaurants = Restaurants(name: name, location: self.location, picture: self.restaurantImageView.image, rating: round(Double(self.restaurantRatingSlider.value)))
            self.addRestaurantCompletionBlock?(restaurant: restaurant)
            //delegate?.viewController(self, didAddRestaurant: restaurant)
            //self.dismissViewControllerAnimated(true, completion: nil)
            
        }
        
    }
    
    @IBAction func cancelButton(sender: UIBarButtonItem) {
        self.presentingViewController?.dismissViewControllerAnimated(true, completion: nil)
    }
}

extension AddRestaurantViewController: UIImagePickerControllerDelegate {
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        self.restaurantImageView.image = info[UIImagePickerControllerOriginalImage] as? UIImage
        dismissViewControllerAnimated(true, completion: nil)
    }
}

extension AddRestaurantViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}