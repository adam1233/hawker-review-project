//
//  NearbyViewController.swift
//  hawker review project
//
//  Created by Kerk Wee Sin on 19/11/2015.
//  Copyright © 2015 Kai siang Tui. All rights reserved.
//

//import UIKit
//import MapKit
//
//class NearbyViewController: UIViewController {
//    var coreLocationManager = CLLocationManager()
//    var locationManager: LocationManager!
//    
//    
//    
//    @IBOutlet weak var mapView: MKMapView!
//    override func viewDidLoad() {
//        super.viewDidLoad()
//        
//        mapView.mapType = .Standard
//        self.coreLocationManager.delegate = self
//        locationManager = LocationManager.sharedInstance
//        let authorizationCode = CLLocationManager.authorizationStatus()
//        
//        if authorizationCode == CLAuthorizationStatus.NotDetermined && coreLocationManager.respondsToSelector("requestAlwaysAuthorization") || coreLocationManager.respondsToSelector("requestWhenInUseAuthorization") {
//            
//            if NSBundle.mainBundle().objectForInfoDictionaryKey("NSLocationAlwaysUsageDescription") != nil {
//                
//                coreLocationManager.requestAlwaysAuthorization()
//            }else{
//                
//                print("No description provided")
//            }
//        }else {
//            
//            getLocation()
//        }
//        // Do any additional setup after loading the view.
//    }
//    func getLocation() {
//        
//        locationManager.startUpdatingLocationWithCompletionHandler { (latitude, longitude, status, verboseMessage, error) -> () in
//            self.displayLocation(CLLocation(latitude: latitude, longitude: longitude))
//        }
//    }
//    
//    
//    
//    func displayLocation(location: CLLocation){
//        mapView.setRegion(MKCoordinateRegion(center: CLLocationCoordinate2DMake(location.coordinate.latitude, location.coordinate.longitude), span: MKCoordinateSpanMake(0.05, 0.05)), animated: true)
//        
//        let locationPinCoordinate = CLLocationCoordinate2DMake(location.coordinate.latitude, location.coordinate.longitude)
//        let annotation = MKPointAnnotation()
//        annotation.coordinate = locationPinCoordinate
//        
//        mapView.addAnnotation(annotation)
//        
//        mapView.showAnnotations([annotation], animated: true)
//        
//        locationManager.reverseGeocodeLocationWithCoordinates(location) { (reverseGecodeInfo, placemark, error) -> Void in
//            print(reverseGecodeInfo)
//        }
//    }
//}
//extension NearbyViewController: MKMapViewDelegate {
//    
//}
//
//extension NearbyViewController: CLLocationManagerDelegate {
//    func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
//        if status != CLAuthorizationStatus.NotDetermined || status == CLAuthorizationStatus.Denied || status == CLAuthorizationStatus.Restricted {
//            getLocation()
//        }
//    }
//}