//
//  RestaurantLoader.swift
//  hawker review project
//
//  Created by Kerk Wee Sin on 05/12/2015.
//  Copyright © 2015 Kai siang Tui. All rights reserved.
//

import Foundation

class RestaurantLoader {
    //singleton pattern
    static let shareLoader: RestaurantLoader = RestaurantLoader()
    private init() {}
    
    // Manage data files
    let fileManager: NSFileManager = NSFileManager.defaultManager()
    
    func dataFileURL() ->NSURL {
    
        let documentsDirectory: NSURL = fileManager.URLsForDirectory(NSSearchPathDirectory.DocumentDirectory, inDomains: NSSearchPathDomainMask.UserDomainMask).first!
        
        let filePath: NSURL = documentsDirectory.URLByAppendingPathComponent("data.plist")
        
        return filePath
    
    }
    
    func readRestaurantFromFile() -> [Restaurants] {
    
        var restaurants: [Restaurants] = []
        
        if let restaurantData: [NSData] = (NSArray(contentsOfURL: self.dataFileURL()) as? [NSData]) {
        
            for data in restaurantData {
            
                let restaurant: Restaurants = NSKeyedUnarchiver.unarchiveObjectWithData(data) as! Restaurants
                restaurants.append(restaurant)
            }
        }
        return restaurants
    }
    
    func saveRestaurantToFile(restaurants: [Restaurants]) {
    
        var restaurantData: [NSData] = []
        for restaurant in restaurants {
        
            let data: NSData = NSKeyedArchiver.archivedDataWithRootObject(restaurant)
            restaurantData.append(data)
        }
        (restaurantData as NSArray).writeToURL(self.dataFileURL(), atomically: true)
    }
    
}