//
//  RestaurantDetailsViewController.swift
//  hawker review project
//
//  Created by Kerk Wee Sin on 27/11/2015.
//  Copyright © 2015 Kai siang Tui. All rights reserved.
//

import UIKit

class RestaurantDetailsViewController: UIViewController {
    
    var restaurant: Restaurants!
    
    @IBOutlet var restaurantRatingLabel: UILabel!
    @IBOutlet var restaurantNameLabel: UILabel!
    @IBOutlet var restaurantImageView: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.restaurantImageView.image = self.restaurant.picture
        self.restaurantNameLabel.text = self.restaurant.name
        
        self.restaurantRatingLabel.text = "The Rating is \(self.restaurant.rating)"
        self.restaurantRatingLabel.textColor = self.variationColorOfText(self.restaurant.rating)
    }
    
    func variationColorOfText(rating: Double) -> UIColor {
        switch rating {
        case 0...1: return UIColor.redColor()
        case 1...2: return UIColor.brownColor()
        case 2...3: return UIColor.orangeColor()
        case 3...4: return UIColor.blueColor()
        case 4...5: return UIColor.greenColor()
        default: return UIColor.blackColor()
        }
    }
}
