//
//  RestaurantsCell.swift
//  hawker review project
//
//  Created by Kerk Wee Sin on 25/11/2015.
//  Copyright © 2015 Kai siang Tui. All rights reserved.
//

import UIKit

class RestaurantsCell: UITableViewCell {
    @IBOutlet var restaurantImageView: UIImageView!

    @IBOutlet var restaurantNameLabel: UILabel!
 
    @IBOutlet var restaurantRatingLabel: UILabel!
    var restaurant: Restaurants! {
    
        didSet {
        
        
            updateDisplay()
        }
    
    }
    
    func updateDisplay() {
        
        self.restaurantNameLabel.text = restaurant.name
        self.restaurantImageView.image = restaurant.picture
        self.restaurantRatingLabel.text = restaurant.rating.description
    
    }

}
