//
//  NearbyViewController.swift
//  hawker review project
//
//  Created by Kerk Wee Sin on 24/11/2015.
//  Copyright © 2015 Kai siang Tui. All rights reserved.
//

import UIKit
import GoogleMaps

class NearbyViewController: UIViewController {
    
//    var likelihoodList: [GMSPlaceLikelihoodList] = []
//    var placesClient: GMSPlacesClient!

    @IBOutlet var addressLabel: UILabel!
    @IBOutlet var mapView: GMSMapView!
    let locationManager: CLLocationManager = CLLocationManager()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //self.placesClient = GMSPlacesClient()
        
        
        
//        let camera = GMSCameraPosition.cameraWithLatitude(-33.86, longitude: 151.20, zoom: 15)
//        self.mapView = GMSMapView.mapWithFrame(CGRectZero, camera: camera)
//
        self.locationManager.delegate = self
        let marker = GMSMarker()
////        marker.position = CLLocationCoordinate2DMake(-33.86, 151.20)
////        marker.title = "Sydney"
////        marker.snippet = "Australia"
        marker.map = mapView
        self.mapView.mapType = kGMSTypeNormal
        self.mapView.settings.compassButton = true
        
        self.locationManager.requestWhenInUseAuthorization()
        self.mapView.delegate = self
      
        
        self.view.bringSubviewToFront(addressLabel)
    }
    
    func reverseGeocodeCoordinate(coordinate: CLLocationCoordinate2D) {
        
       
        let geocoder = GMSGeocoder()
        
        
        geocoder.reverseGeocodeCoordinate(coordinate) { response, error in
            if let address = response?.firstResult() {
                
              
                let lines = address.lines as! [String]
                self.addressLabel.text = lines.joinWithSeparator("\n")
                //let labelHeight = self.addressLabel.intrinsicContentSize().
                self.mapView.padding = UIEdgeInsets(top: self.topLayoutGuide.length, left: 0, bottom: 50, right: 0)
                
                
                UIView.animateWithDuration(0.25) {
                
                    self.view.layoutIfNeeded()
                }
            }
        }
    }
}

extension NearbyViewController: GMSMapViewDelegate {

    func mapView(mapView: GMSMapView!, idleAtCameraPosition position: GMSCameraPosition!) {
        reverseGeocodeCoordinate(position.target)
    }
}

extension NearbyViewController: CLLocationManagerDelegate {
    
    func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        
        if status == .AuthorizedWhenInUse {
            
            
            manager.startUpdatingLocation()
            
            
            self.mapView.myLocationEnabled = true
            self.mapView.settings.myLocationButton = true
        }
    }

    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.first {
            
          
            mapView.camera = GMSCameraPosition(target: location.coordinate, zoom: 15, bearing: 0, viewingAngle: 0)
            
         
            manager.stopUpdatingLocation()
        }
        
    }
    func mapView(mapView: GMSMapView!, didTapAtCoordinate coordinate: CLLocationCoordinate2D) {
        print("Tapped at Coordinate latitude:\(coordinate.latitude), Longitude:\(coordinate.longitude)")
    }
}