//
//  RestaurantAPITest.swift
//  hawker review project
//
//  Created by Kerk Wee Sin on 14/12/2015.
//  Copyright © 2015 Kai siang Tui. All rights reserved.
//

import XCTest
import MapKit
@testable import hawker_review_project
class RestaurantAPITest: XCTestCase {
    
    var session: NSURLSession!
    
    
    override func setUp() {
        super.setUp()
        
        let config: NSURLSessionConfiguration = NSURLSessionConfiguration.defaultSessionConfiguration()
        
        config.HTTPAdditionalHeaders = ["X-Parse-Application-Id" : "ldh8z7k8iko1990ArZaGBBxIvYoWkUmxhq0tr6Cv", "X-Parse-REST-API-Key": "cStfEU0H5cUqbiyTk0vYF44WTbj8jAcT4QPodsum", "Content-Type": "application/json"]
        
        self.session = NSURLSession(configuration: config)
       
        
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testLoadHomepage() {
    
        let expectation = self.expectationWithDescription("Load Task")
        let session: NSURLSession = NSURLSession.sharedSession()
        let url: NSURL = NSURL(string: "https://www.apple.com")!
        
        let loadTask: NSURLSessionDataTask = self.session.dataTaskWithURL(url) { (data:NSData?, response:NSURLResponse?, error: NSError?) -> Void in
            
            
            let htmlString: String? = String(data: data!, encoding: NSUTF8StringEncoding)
            XCTAssert(htmlString != nil, "should have received some html String from server")
            NSLog("task completed")
            expectation.fulfill()
        }
        
        loadTask.resume()
        
        self.waitForExpectationsWithTimeout(10, handler: nil)
    }
    
    func testLoadingApi() {
    
        let expectation = self.expectationWithDescription("Load Task")
        let url: NSURL = NSURL(string: "https://api.parse.com/1/classes/Restaurants")!
        
        let loadTask: NSURLSessionDataTask = self.session.dataTaskWithURL(url)
            { (data: NSData?, response: NSURLResponse?, error: NSError?) -> Void in
            NSLog("task completed")
            expectation.fulfill()
            
            let httpCode: Int = (response as! NSHTTPURLResponse).statusCode
            XCTAssert(httpCode == 200, "server should have return 200 content OK")
        }
    
            loadTask.resume()
            self.waitForExpectationsWithTimeout(10, handler: nil)
    }

    
    func testLoadingJSONDataFromAPI() {
        
        var restaurants: [Restaurants] = []
        
        let expectation = self.expectationWithDescription("Load Task")
      
        let url: NSURL = NSURL(string: "https://api.parse.com/1/classes/Restaurants")!
       
        
        let loadTask: NSURLSessionDataTask = self.session.dataTaskWithURL(url) { (data: NSData?, response: NSURLResponse?, error: NSError?) -> Void in
            
            let httpCode: Int = (response as! NSHTTPURLResponse).statusCode
            XCTAssert(httpCode == 200, "server should have return 200 content OK")
            
            let json: [String: AnyObject] = try! NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions(rawValue: 0)) as! [String: AnyObject]
            
            if let resultsArray: [ [String: AnyObject] ] = json["results"] as? [ [String: AnyObject] ]{
            
                for jsonData in resultsArray {
                
                    let name: String = jsonData["name"] as! String
                    let objectId: String = jsonData["objectId"] as! String
                    let picture: String = jsonData["Picture"] as! String
                    let rating: Double = jsonData["Rating"] as! Double
                    let latitude: Double = jsonData["Latitude"] as! Double
                    let longitude: Double = jsonData["Longitude"] as! Double
                    let restaurant: Restaurants = Restaurants(name: name, location:CLLocationCoordinate2DMake(latitude, longitude), picture: UIImage(named: picture), rating: rating)
                    
                    restaurants.append(restaurant)
                    
                }
            }
            XCTAssert(restaurants.count > 0, "Restaurant shouldnt be empty")
            expectation.fulfill()
        
        }
        
        loadTask.resume()
        self.waitForExpectationsWithTimeout(10, handler: nil)
    }

    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measureBlock {
            // Put the code you want to measure the time of here.
        }
    }
}
